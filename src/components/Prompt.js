import React, { Component } from 'react'
import '../assets/styling/index.css'
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody } from 'mdbreact'

class ModalPage extends Component {
  state = {
    modal: false
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    })
  }

  render() {
    return (
      <MDBContainer>
        <MDBModal isOpen={this.toggle} toggle={this.toggle} size='sm'>
          <MDBModalBody>
            <p>
              Film<span title='This film was a great movie!'> movie title </span>added to favorites
            </p>
          </MDBModalBody>
          <MDBBtn color='secondary' onClick={this.toggle} className='text-bold'>
            Close
          </MDBBtn>
        </MDBModal>
      </MDBContainer>
    )
  }
}

export default ModalPage
