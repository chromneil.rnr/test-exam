import React from 'react'
import '../assets/styling/index.css'
import { MDBContainer, MDBRow, MDBCol } from 'mdbreact'
import { connect } from 'react-redux'
import { getFilmDetails } from '../reducer/getFilm'
import { getSpecificFilmDetails } from '../reducer/getFilm'

const details = 'FILM DETAILS'

class FilmDetails extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      specificFilmData: []
    }
  }

  componentDidMount = () => {
    this.handleViewSpecific()
  }

  handleViewSpecific = async () => {
    await this.props.getSpecificFilmDetails(this.props.match.params.id)
    this.setState({ specificFilmData: this.props.getSpecificFilmState })
  }

  render() {
    let { specificFilmData } = this.state
    return (
      <MDBContainer className='background-color'>
        <p className='text-center text-bold mt-3'>{details}</p>
        <MDBCol className='py-3'>
          <p className='font-family text-bold'>
            Title: <span className='text-italic'>{specificFilmData.title} </span>
          </p>
          <p className='font-family text-bold'>
            Director:<span className='text-italic'> {specificFilmData.director} </span>
          </p>
          <p className='font-family text-bold'>
            Producer: <span className='text-italic'>{specificFilmData.producer} </span>
          </p>
          <hr />
          <p className='font-family text-bold'>
            Created: <span className='text-italic'>{specificFilmData.created} </span>
          </p>
          <p className='font-family text-bold'>
            Release_date: <span className='text-italic'>{specificFilmData.release_date} </span>
          </p>
          <hr />
          <p className='font-family text-bold'>
            Edited:<span className='text-italic'> {specificFilmData.edited} </span>
          </p>
          <p className='font-family text-bold'>
            Episode_id: <span className='text-italic'>{specificFilmData.episode_id} </span>
          </p>
          <hr />
          <p className='font-family text-bold'>
            Planets:<span className='text-italic'> {specificFilmData.planets} </span>
          </p>
          <p className='font-family text-bold'>
            Species: <span className='text-italic'>{specificFilmData.species} </span>
          </p>
          <p className='font-family text-bold'>
            Starships:<span className='text-italic'> {specificFilmData.starships} </span>
          </p>
          <hr />
          <p className='font-family text-bold'>
            Vehicles: <span className='text-italic'>{specificFilmData.vehicles} </span>
          </p>
          <p className='font-family text-bold'>
            Opening_crawl: <span className='text-italic'>{specificFilmData.opening_crawl} </span>
          </p>
          <p className='font-family text-bold'>
            Url: <span className='text-italic'>{specificFilmData.url} </span>
          </p>
          <hr />
          <p className='font-family text-bold'>
            Characters:<span className='text-italic'> {specificFilmData.characters} </span>
          </p>
        </MDBCol>
      </MDBContainer>
    )
  }
}

const mapStateToProps = state => {
  let getFilm = state.getFilm
  return {
    getFilmState: getFilm.getFilmState,
    getSpecificFilmState: getFilm.getSpecificFilmState
  }
}

const mapDispatchToProps = {
  getFilmDetails,
  getSpecificFilmDetails
}

export default connect(mapStateToProps, mapDispatchToProps)(FilmDetails)
