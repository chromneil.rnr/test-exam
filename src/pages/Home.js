import React from 'react'
import '../assets/styling/index.css'
import { MDBContainer, MDBRow, MDBIcon } from 'mdbreact'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { getFilmDetails } from '../reducer/getFilm'
import { getSpecificFilmDetails } from '../reducer/getFilm'

class Dashbord extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false,
      search: '',
      favorite: '',
      filmsData: [],
      displayFilm: [],
      displaySpecfic: [],
      specificFilmData: ''
    }
  }

  componentWillMount() {
    this.handleGetFilmDetails()
  }

  handleChange = event => {
    let { filmsData } = this.state
    let keyword = event.target.value
    let searchItems = []
    if (keyword.length > 0) {
      filmsData.map(item => {
        if (item.title.toLowerCase().includes(keyword.toLowerCase())) {
          searchItems.push(item)
        }
      })
    } else {
      searchItems = filmsData
    }
    // this.setState({filmsData: searchItems})
    let fav = localStorage.getItem('favorites')
    fav = JSON.parse(fav)
    this.renderFilmsList(fav, searchItems)
  }

  handleGetFilmDetails = async data => {
    await this.props.getFilmDetails(data)
    this.setState({ filmsData: this.props.getFilmState.results })
    let fav = localStorage.getItem('favorites')
    fav = JSON.parse(fav)
    this.renderFilmsList(fav, this.props.getFilmState.results)
  }

  handleViewSpecific = async data => {
    await this.props.getSpecificFilmDetails(data)
    this.setState({ specificFilmData: this.props.getSpecificFilmState })
    this.handleGetFilmDetails(this.state.specificFilmData)
  }

  renderFilmsList = (fav, data) => {
    let temp = []
    if (fav) {
      data.map(val => {
        if (fav[val.episode_id] === undefined) {
          val = { ...val, favorite: 1 }
        } else {
          val = { ...val, favorite: fav[val.episode_id] }
        }
        temp.push(val)
      })
      temp.sort((a, b) => {
        return a.favorite - b.favorite
      })
    } else {
      temp = data
    }
    let displayFilm = temp.map((value, index) => {
      let isFav = value.favorite === 0 ? 'color-green' : ''
      return (
        <MDBContainer key={index}>
          <MDBRow className='justify-content-start py-1 border mt-1'>
            <span
              title='Click here to add film to favorites!'
              className={`${isFav} favorite `}
              onClick={() => {
                this.handleAddfavorite(value.episode_id)
              }}>
              <MDBIcon far icon='star' />
            </span>
            <Link to={`/films/${value.url.split('/')[5]}`}>
              <span className='border font-size-14 font-weight-400 font-family'>{value.title}</span>
            </Link>
          </MDBRow>
        </MDBContainer>
      )
    })
    this.setState({ displayFilm })
  }

  handleAddfavorite = id => {
    let fav = localStorage.getItem('favorites')
    fav = JSON.parse(fav)
    if (fav === null) {
      localStorage.setItem('favorites', JSON.stringify({ [id]: 0 }))
    } else {
      if (fav[id === undefined]) {
        fav = { ...fav, [id]: 0 }
        localStorage.setItem('favorites', JSON.stringify(fav))
      } else {
        fav[id] = fav[id] === 0 ? 1 : 0
        localStorage.setItem('favorites', JSON.stringify(fav))
      }
    }
    alert('Added to favorite.')
    let { filmsData } = this.state
    this.renderFilmsList(fav, filmsData)
  }

  render() {
    let { filmsData, displayFilm } = this.state
    return (
      <MDBContainer className='background-color '>
        <MDBRow className='justify-content-center py-3 '>
          <input
            className='font-family text-center'
            placeholder='search here'
            type='text'
            name='search'
            onChange={e => this.handleChange(e)}
          />
          {filmsData && displayFilm}
        </MDBRow>
      </MDBContainer>
    )
  }
}

const mapStateToProps = state => {
  let getFilm = state.getFilm
  return {
    getFilmState: getFilm.getFilmState,
    getSpecificFilmState: getFilm.getSpecificFilmState
  }
}

const mapDispatchToProps = {
  getFilmDetails,
  getSpecificFilmDetails
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashbord)
